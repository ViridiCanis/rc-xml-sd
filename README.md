# rc-xml-sd

## Setup

### Ringcentral App

Go to your Ringcentral Developer Account and register create an App with the
following settings:

- App Type: REST Api App
- Auth:
  - Password-based auth flow
  - Issue refresh token: yes
- Security -> App Permissions: Read Accounts, Read Messages, SMS, Read Call Log

then go to the Dashboard of the App and copy the Client ID and the Client Secret
into the file `.env` to the corresponding variables `RC_CLIENT_ID` and `RC_CLIENT_SECRET`

### OMM

in `.env` set:

replace the ip of 'your pc' and of the omm with localhost if running on the omm

```js
BASE_URL  = "http://[ip of your pc]:5000";
OMM_URL   = "[ip of the omm]";
OMM_USER  = "[omm user]";
OMM_PW    = "[omm password]";
```

make sure the firewall of your pc allows TCP on port 5000

if configuring it for using the program on the omm use localhost as the server address

for all the following settings set the port to 5000

add to XML Applications your pc as the server with protocol set to HTTP
and set the path respectively to:
- for reading messages:   `get-messages?usernum={number}`
- for logging out:        `logout?usernum={number}`
- for sending a message:  `send-message-prompt?usernum={number}`

for directory:
- make an entry with type XML and Path `directory?usernum={number}`
for call logs:
- change Caller List and Redial List in XML Applications to the Path `call-log?usernum={number}`

### User Authentication

in the settings for each user of the OMM set under User service:
- Authentication name: the email address or phone number (in E.164 format) of the corresponding Ringcentral user
- Password: the password of the user on Ringcentral

e.g. the same as when logging in at Ringcentral

## Usage

you need yarn for this project
to install it follow [this guide](https://yarnpkg.com/getting-started/install)

to get all dependencies:

```sh
$ yarn
```

to start the server:

```sh
$ yarn node .
```

To stop the server press CTRL+C in the terminal.

## getting it on the omm

using the node executable for armv7 from the ([Node Download Page](https://nodejs.org/en/download/)) and putting it in dist/

this requires ncc (installed using `npm i -g ncc`)

example commands:
```
ncc build index.js
cp .env dist/
cd dist/
tar caf rc.tar.gz .env node index.js
```
move the resulting .tar.gz to the omm
```
mkdir rc
tar xf rc.tar.gz -C rc
cd rc
./node index.js
```
