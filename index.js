require("dotenv").config();
const log4js = require("log4js");
log4js.configure({
  appenders: {
    console: { type: "console" }
  },
  categories: {
    default: { appenders: ["console"], level: "info" }
  }
});

const logger = log4js.getLogger();

let authTokens = new Map();
let syncTokens = new Map();
let hasNotification = [];

const AXI_Client = require("./AXI_Client");
let axi = new AXI_Client(process.env.OMM_PORT, process.env.OMM_URL, process.env.OMM_USER, process.env.OMM_PW, () => {
  logger.warn("AXI connection closed");
}, logger)

const RC = require("@ringcentral/sdk").SDK;
let rcsdk = new RC({
  clientId: process.env.RC_CLIENT_ID,
  clientSecret: process.env.RC_CLIENT_SECRET,
  server: process.env.RC_SERVER_URL,
});

// webserver for providing the endpoints
let app = require("express")();

// disable caching (status code 304 Not Modified)
app.disable("etag");

let server = require("http").createServer(app);
server.listen(5000);
logger.info("listening to port 5000");

/**
 * endpoint for logging out
 */
app.get("/logout", async (req, res) => {
  logger.info("GET /logout");

  let user_token = authTokens.get(req.query.usernum);

  if (user_token) {
    let platform = rcsdk.platform();
    platform.auth().setData(JSON.parse(user_token));

    platform.logout();

    authTokens.delete(req.query.usernum);
    syncTokens.delete(req.query.usernum);
  }

  res.status(200).end();
});

/**
 * endpoint for getting all messages
 */
app.get("/get-messages", async (req, res) => {
  logger.info("GET /get-messages");

  let notiIndex = hasNotification.indexOf(req.query.usernum);
  if (notiIndex != -1) {
    // clear idle line
    axi.request({
      PushXML: {
        _attributes: {
          num: req.query.usernum,
          xmlDoc: {
            AastraIPPhoneConfiguration: {
              ConfigurationItem: {
                Parameter: "Set idle line",
                Value: ""
              }
            }
          }
        }
      }
    });

    hasNotification.splice(notiIndex, 1);
  }

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      user_token = await login(req.query.usernum);
    } catch (e) {
      logger.error(`failed logging in for ${req.query.usernum}`, e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  let platform = rcsdk.platform();
  platform.auth().setData(JSON.parse(user_token));
  if (platform.loggedIn()) {
    try {
      let resp = await platform.get(
        "/restapi/v1.0/account/~/extension/~/message-store",
        {
          messageType: "SMS",
          dateFrom: "2000-01-01",
        }
      );
      let jsonObj = await resp.json();

      res.append("Content-Type", "application/xml");
      res.send(json_to_xml_get(jsonObj, req.query.usernum));
    } catch (e) {
      logger.error("failed getting messages", e);

      res.status(500).end();
    }
  }

  res.status(500).end();
});

/**
 * endpoint for getting a specific message
 */
app.get("/get-message-by-id", async (req, res) => {
  logger.info("GET /get-message-by-id");

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      user_token = await login(req.query.usernum);
    } catch (e) {
      logger.error("failed logging in", e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  let platform = rcsdk.platform();
  platform.auth().setData(JSON.parse(user_token));
  if (platform.loggedIn()) {
    try {
      let resp = await platform.get(
        `/restapi/v1.0/account/~/extension/~/message-store/${req.query.id}`
      );
      let jsonObj = await resp.json();
      res.append("Content-Type", "application/xml");
      res.send(json_to_xml_message(jsonObj, req.query.usernum));
    } catch (e) {
      logger.error("failed getting message by id", e);
    }
  }

  res.status(500).end();
});

/**
 * endpoint for the prompt for sending messages
 */
app.get("/send-message-prompt", async (req, res) => {
  logger.info("GET /send-message-prompt");

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      await login(req.query.usernum);
    } catch (e) {
      logger.error("failed logging in", e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  res.append("Content-Type", "application/xml");
  res.send(send_prompt_xml(req.query.usernum, req.query.to));
});

/**
 * endpoint for sending the message
 * receives get additional parameter: to
 */
app.get("/send-message", async (req, res) => {
  logger.info("GET /send-message");

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      user_token = await login(req.query.usernum);
    } catch (e) {
      logger.error("failed logging in", e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  let platform = rcsdk.platform();
  platform.auth().setData(JSON.parse(user_token));
  if (platform.loggedIn()) {
    // get the phone number for the logged in user
    let res_from = await platform.get(
      "/restapi/v1.0/account/~/extension/~/phone-number"
    );
    let from = (await res_from.json()).records[0].phoneNumber;
    let to = req.query.to;
    let text = req.query.text;

    try {
      let resp = await platform.post(
        "/restapi/v1.0/account/~/extension/~/sms",
        {
          from: { phoneNumber: from },
          to: [{ phoneNumber: to }],
          text: text,
        }
      );
      let jsonObj = await resp.json();
      logger.info("send message", jsonObj.messageStatus);
      res.send(messsage_sent_xml());
    } catch (e) {
      logger.error("failed sending message", e);

      res.status(500).end();
    }
  }
});

/**
 * endpoint for the directory
 * receives additional optional parameters: entry, page
 */
app.get("/directory", async (req, res) => {
  logger.info("GET /directory");

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      user_token = await login(req.query.usernum);
    } catch (e) {
      logger.error("failed logging in", e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  let platform = rcsdk.platform();
  platform.auth().setData(JSON.parse(user_token));
  if (platform.loggedIn()) {
    if (req.query.entry) {
      try {
        let resp = await platform.get(`/restapi/v1.0/account/~/directory/entries/${req.query.entry}`);

        let jsonObj = await resp.json();

        res.header("Content-Type", "application/xml");
        res.send(directory_entry_xml(jsonObj));
      } catch (e) {
        logger.error("failed getting directory entry", e);

        res.status(500).end();
      }
    } else {
      if (!req.query.page) {
        req.query.page = 1;
      }

      try {
        let resp = await platform.get(`/restapi/v1.0/account/~/directory/entries?perPage=7&page=${req.query.page}`);

        let jsonObj = await resp.json();

        res.header("Content-Type", "application/xml");
        res.send(directory_xml(jsonObj, req.query.usernum));
      } catch (e) {
        logger.error("failed getting directory", e);

        res.status(500).end();
      }
    }
  }
})

/**
 * endpoint for the call-log
 * receives additional optional parameter: page
 */
app.get("/call-log", async (req, res) => {
  logger.info("GET /call-log");

  let user_token = authTokens.get(req.query.usernum);

  if (!user_token) {
    try {
      user_token = await login(req.query.usernum);
    } catch (e) {
      logger.error("failed logging in", e);

      res.append("Content-Type", "application/xml");
      res.send(error_login_xml());
      return;
    }
  }

  let platform = rcsdk.platform();
  platform.auth().setData(JSON.parse(user_token));
  if (platform.loggedIn()) {
    if (!req.query.page) {
      req.query.page = 1;
    }

    try {
      let resp = await platform.get(`/restapi/v1.0/account/~/extension/~/call-log?perPage=7&page=${req.query.page}`);

      let jsonObj = await resp.json();

      res.header("Content-Type", "application/xml");
      res.send(call_log_xml(jsonObj, req.query.usernum));
    } catch (e) {
      logger.error("failed getting call log", e);

      res.status(500).end();
    }
  }
})

/**
 *
 * @param {string} usernum number of the user to login
 * @returns {Promise<string>} JSON of the tokens returned by the SDK
 */
async function login(usernum) {
  if (!usernum) {
    throw new Error("missing usernum in request")
  }

  logger.info(`logging in ${usernum}`);

  let axi_resp = await axi.request({
    GetPPUserByNumber: {
      _attributes: {
        num: usernum
      }
    }
  });
  let user = axi_resp.data.GetPPUserByNumberResp.user._attributes;

  let username = user.serviceAuthName;
  let password = user.serviceAuthPassword;

  let resp;
  try {
    resp = await rcsdk.platform().login({
      username: username,
      password: password,
    });
  } catch (e) {
    logger.error("failed logging in", e);

    if (JSON.parse(e.response.body.read()).error == "invalid_grant") {
      throw new Error("invalid credentials");
    }
  }

  let token = JSON.stringify(await resp.json())
  authTokens.set(usernum, token);

  resp = await rcsdk.platform().get("/restapi/v1.0/account/~/extension/~/message-sync", {
    syncType: "FSync",
    messageType: "SMS"
  });

  let json = await resp.json();
  syncTokens.set(usernum, json.syncInfo.syncToken);

  return token;
};

let sync_interval = setInterval(async () => {
  /*
  TODO:
  - doesn't account for whether the message is sent or received
  - is independent of whether using get-message the message has already been read
  - BUG: a sync can fail due to "connection refused"
    - fixed for now by deleting authtoken, requiring a new login
  */
  logger.info("starting sync")

  let platform = rcsdk.platform();

  for (let [usernum, token] of authTokens) {
    platform.auth().setData(JSON.parse(token));
    if (platform.loggedIn()) {
      let json;

      try {
        let resp = await rcsdk.platform().get("/restapi/v1.0/account/~/extension/~/message-sync", {
          syncType: "ISync",
          syncToken: syncTokens.get(usernum)
        });

        json = await resp.json();
      } catch (e) {
        logger.error("failed sync", e);
        authTokens.delete(usernum);
        return;
      }

      syncTokens.set(usernum, json.syncInfo.syncToken);

      let records = json.records;
      if (records.length > 0) {
        if (hasNotification.indexOf(usernum) == -1) {
          await axi.request({
            PushXML: {
              _attributes: {
                num: usernum,
                xmlDoc: {
                  AastraIPPhoneConfiguration: {
                    ConfigurationItem: {
                      Parameter: "Set idle line",

                      // can't add Icon to PhoneConfiguration (Icon:Envelope)
                      Value: "new message"
                    }
                  }
                }
              }
            }
          });

          hasNotification.push(usernum);
        }
      }
    }
  }
}, 1 * 60 * 1000);

/**
 * helper function, which returns the XML for the error page
 *
 * @returns {string} XML
 */
function error_login_xml() {
  return `<?xml version="1.0" encoding="UTF-8"?>
    <AastraIPPhoneTextScreen>
      <Title>RC SMS</Title>
      <Text>failed logging in</Text>
    </AastraIPPhoneTextScreen>
    `;
}

/**
 * helper function, which returns the name or the phone number depending on what's available
 *
 * @param {object} json user entry
 * @returns {string}
 */
function phoneNumber_or_name(json) {
  return json.name ? json.name : " " + json.phoneNumber;
}

/**
 * helper function, which returns the XML for the message list
 *
 * @param {object} json response from the RC SDK
 * @param {object} usernum the user who sent the request
 * @returns {string} XML
 */
function json_to_xml_get(json, usernum) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneTextMenu defaultIndex="1">
    <Title>RC SMS</Title>
  `;

  for (let message of json.records) {
    xml += `<MenuItem>
      <URI>${process.env.BASE_URL}/get-message-by-id?id=${message.id}&amp;usernum=${usernum}</URI>
      <Prompt>${phoneNumber_or_name(message.from)}</Prompt>
    </MenuItem>`;
  }

  xml += `
  </AastraIPPhoneTextMenu>`;

  return xml;
}

/**
 * helper function, which returns the XML for the a message screeen
 *
 * @param {object} json response from the RC SDK
 * @param {object} usernum the user who sent the request
 * @returns {string} XML
 */
function json_to_xml_message(json, usernum) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextScreen>
  <Title>${phoneNumber_or_name(json.from)}</Title>
  <Text>${json.subject}</Text>
  <SoftKey index="1">
    <Label>Delete</Label>
    <URI>${process.env.BASE_URL}/get-messages?usernum=${usernum}</URI>
  </SoftKey>
</AastraIPPhoneTextScreen>`;

  return xml;
}

/**
 * helper function, which returns the XML for the prompt for sending messages
 *
 * @param {object} usernum the user who sent the request
 * @param {object} to to who to send the user, existence determines which prompt to use
 * @returns {string} XML
 */
function send_prompt_xml(usernum, to) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneInputScreen type="${to ? "string" : "number"}">
    <Title>RC SMS</Title>`;

  if (!to) {
    xml += `<URL>${process.env.BASE_URL}/send-message-prompt?usernum=${usernum}</URL>
      <Prompt>Receiving Number</Prompt>
      <Parameter>to</Parameter>`;
  } else {
    xml += `<URL>${process.env.BASE_URL}/send-message?usernum=${usernum}&to=${to}</URL>
      <Prompt>Text</Prompt>
      <Parameter>text</Parameter>`;
  }

  return xml + `</AastraIPPhoneInputScreen>`;
}

/**
 * helper function, which returns the XML for the message sent confirmation
 *
 * @returns {string} XML
 */
function messsage_sent_xml() {
  return `<?xml version="1.0" encoding="UTF-8"?>
  <AastraIPPhoneTextScreen>
    <Title>RC SMS</Title>
    <Text>message sent</Text>
  </AastraIPPhoneTextScreen>
  `;
}

function directory_xml(json, usernum) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
  <Title wrap="no"> Directory</Title>`

  for (let record of json.records) {
    xml += `<MenuItem base="">`
    if (record.firstName) {
      xml += `<Prompt>${record.firstName} ${record.lastName}</Prompt>`
    } else {
      xml += `<Prompt>${record.name}</Prompt>`
    }

    xml += `<URI>${process.env.BASE_URL}/directory?usernum=${usernum}&entry=${record.id}</URI>`

    xml += `<Selection>info</Selection>
    </MenuItem > `;
  }

  if (json.paging.page < json.paging.totalPages) {
    xml += `<MenuItem>
      <Prompt>Next Page</Prompt>
      <URI>${process.env.BASE_URL}/directory?usernum=${usernum}&page=${json.paging.page + 1}</URI>
    </MenuItem>`
  }

  return xml + `</AastraIPPhoneTextMenu>`;
}

function directory_entry_xml(json) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
  <Title wrap="no">`

  if (json.firstName) {
    xml += `${json.firstName} ${json.lastName}`
  } else {
    xml += `${json.name}`
  }
  xml += `</Title>`;

  if (json.phoneNumbers) {
    for (let phone of json.phoneNumbers) {
      xml += `<MenuItem base="">
      <Prompt>`

      if (phone.label) {
        xml += phone.label
      } else {
        xml += phone.type
      }
      xml += ` ${phone.formattedPhoneNumber}`

      xml += `</Prompt>
      <Selection>info</Selection>
      <Dial>${phone.phoneNumber}</Dial>
      </MenuItem > `;
    }
  }

  return xml + `</AastraIPPhoneTextMenu>`;
}

function call_log_xml(json, usernum) {
  let xml = `<?xml version="1.0" encoding="UTF-8"?>
<AastraIPPhoneTextMenu defaultIndex="1" destroyOnExit="yes" Timeout="50">
  <Title wrap="no">Call Log</Title>`

  for (let record of json.records) {
    xml += `<MenuItem base="">
    <Prompt>`

    if (record.direction == "Inbound") {
      xml += `i ${phoneNumber_or_name(record.from)}`;
    } else {
      xml += `o ${phoneNumber_or_name(record.to)}`;
    }
    xml += `</Prompt>`;

    xml += `<Dial>`;
    if (record.direction == "Inbound") {
      xml += record.from.phoneNumber;
    } else {
      xml += record.to.phoneNumber;
    }
    xml += `</Dial>`;

    xml += `<Selection>info</Selection>
    </MenuItem > `;
  }

  if (json.paging.page < json.paging.totalPages) {
    xml += `<MenuItem>
      <Prompt>Next Page</Prompt>
      <URI>${process.env.BASE_URL}/call-log?usernum=${usernum}&page=${json.paging.page + 1}</URI>
    </MenuItem>`
  }

  return xml + `</AastraIPPhoneTextMenu>`;
}

async function shutdown() {
  clearInterval(sync_interval);

  for (let usernum of hasNotification) {
    axi.request({
      PushXML: {
        _attributes: {
          num: usernum,
          xmlDoc: {
            AastraIPPhoneConfiguration: {
              ConfigurationItem: {
                Parameter: "Set idle line",
                Value: ""
              }
            }
          }
        }
      }
    });
  }

  server.close(() => {
    logger.warn("Server terminated");
  });
  axi.end();
}

process.on("SIGINT", shutdown);
process.on("SIGTERM", shutdown);
