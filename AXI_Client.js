const xml_js = require("xml-js");
const net = require("net");

const forge = require("node-forge");
const pki = forge.pki;
const rsa = pki.rsa;
const BigInteger = forge.jsbn.BigInteger;

class AXIClient {
  #callbacks;
  #seq;
  #chunk;

  /**
   * creates a connection to the AXI Interface of the OMM
   *
   * @param {number} port
   * @param {string} host
   * @param {string} username
   * @param {string} password
   * @param logger
   * @param {function(boolean)} close_callback called when the connection gets closed
   * receives `boolean` argument `hadError`
   */
  constructor(port, host, username, password, close_callback, logger) {
    this.logger = logger;
    this.connected = false;
    this.#seq = 0;
    this.#chunk = "";
    this.#callbacks = new Map();

    this.socket = net.connect(port, host, () => {
      // authenticate as soon as the connection is established
      this.#send(`<Open username="${username}" password="${password}" seq="10000"/>`);
    });

    // keep the connection alive
    // every 5 min (- 5 sec to ensure the request reaches the OMM)
    this.ping_interval = setInterval(() => {
      try {
        this.request({ Ping: {} });
      } catch (e) {}
    }, 5 * 60000 - 5000);

    this.socket.on("close", (hadError) => {
      this.connected = false;
      clearInterval(this.ping_interval);
      close_callback(hadError);
    });

    this.socket.on("data", (data) => {
      this.#data_handler(data);
    })

    // callback for the response of the Open request
    this.#callbacks.set("10000", (resp) => {
      this.logger.info("AXI Connected");

      let attrs = resp.data.OpenResp.publicKey._attributes;

      let n = new BigInteger(attrs.modulus, 16);
      let e = new BigInteger(attrs.exponent, 16);

      this.public_key = rsa.setPublicKey(n, e);

      this.connected = true;
    });
  }

  /**
   * helper function to ensure the trailing null byte
   *
   * @param {string} data data to send
   */
  #send(data) {
    this.socket.write(data + "\0");
  }

  /**
   * handler for all incoming data
   *
   * @param {Buffer} data received binary data
   */
  #data_handler(data) {
    let text = data.toString();

    this.#chunk += text;
    if (!this.#chunk.endsWith("\0")) {
      return;
    }

    let resp = xml_js.xml2js("<data>" + this.#chunk + "</data>", { compact: true });
    this.#chunk = "";

    // find the type of the response (anything with no starting `_`
    // in compact mode of xml - js)
    let type = Object.keys(resp.data).find((val) => {
      return !val.startsWith("_");
    });

    let seq = resp.data[type]._attributes.seq;
    let callback = this.#callbacks.get(seq);

    if (callback) {
      callback(resp);
      this.#callbacks.delete(seq);
    } else {
      this.logger.warning("Unexpected response type", type);
    }
  }

  /**
   * sends the request to close the connection to the omm
   */
  end() {
    this.socket.end();
  }

  /**
   * send a request to the OMM,
   * returning a Promise which resolves when the corresponding response is received
   *
   * ensure the connection (`client.connected`) is established before calling this method
   *
   * @param {object} data_json request as a JS Object
   * @returns {Promise<object>} response as a JS Object
   *
   * @throws {Error} the client must have an active connection to the OMM
   * @throws {Error} data_json must have a Request Type
   *
   * @example
   * let resp = await client.request({
   *    GetExampleRequest: {
   *        _attributes: {
   *          exampleAttribute: "1234"
   *        },
   *        emptyExampleChild: {}
   *    }
   * })
   */
  request(data_json) {
    if (!this.connected) {
      throw Error("not connected");
    }

    // find the type of the request (anything with no starting `_`
    // in compact mode of xml - js)
    // to be able to add the sequence number to the request
    let type = Object.keys(data_json).filter((key) => !key.startsWith("_"))[0];
    let seq = this.#seq++;

    // ensure it's has a request type
    if (!data_json[type]) {
      throw new Error("missing request type");
    }

    // ensure _attributes exists
    if (!data_json[type]._attributes) {
      data_json[type]._attributes = {};
    }

    data_json[type]._attributes.seq = seq;

    if (type == "PushXML") {
      let inner = data_json[type]._attributes.xmlDoc;
      inner._declaration = {
        _attributes: {
          version: "1.0",
          encoding: "UTF-8"
        }
      }
      data_json[type]._attributes.xmlDoc = xml_js.js2xml(inner, { compact: true });
    }

    let data = xml_js.js2xml(data_json, { compact: true });

    this.#send(data);

    return new Promise((res, rej) => {
      this.#callbacks.set(seq.toString(), (data) => {
        res(data);
      })
    })
  }
}

module.exports = AXIClient
